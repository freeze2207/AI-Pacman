using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : GhostBaseState
{
    public List<Vector3> patrolPoints;

    public int predictOffset = 2;

    public float distance = 2.0f;
    public Vector2 mapTopLeft;
    public Vector2 mapBottomRight;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController.state = GhostController.GhostState.Chase;
        
        switch (ghostController.gameObject.name)
        {
            case "Blinky":
                ghostController.pathCompletedEvent.AddListener(BlinkyChase);
                ghostController.pathCompletedEvent.Invoke();
                break;
            case "Clyde":
                ghostController.pathCompletedEvent.AddListener(ClydeChase);
                ghostController.pathCompletedEvent.Invoke();
                break;
            case "Inky":
                ghostController.pathCompletedEvent.AddListener(InkyChase);
                ghostController.pathCompletedEvent.Invoke();
                break;
            case "Pinky":
                ghostController.pathCompletedEvent.AddListener(PinkyChase);
                ghostController.pathCompletedEvent.Invoke();
                break;
            default:
                break;
        }

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (ghostController.pathIndex > ghostController.path.Count)
        {
            ghostController.path.Clear();
            ghostController.pathIndex = 0;
            ghostController.pathCompletedEvent.Invoke();
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsChase", false);
        
        switch (ghostController.gameObject.name)
        {
            case "Blinky":
                ghostController.pathCompletedEvent.RemoveListener(BlinkyChase);
                break;
            case "Clyde":
                ghostController.pathCompletedEvent.RemoveListener(ClydeChase);
                break;
            case "Inky":
                ghostController.pathCompletedEvent.RemoveListener(InkyChase);
                break;
            case "Pinky":
                ghostController.pathCompletedEvent.RemoveListener(PinkyChase);
                break;
            default:
                break;
        }
    }

    // Chase the Pacman
    private void BlinkyChase()
    {
        ghostController.SetMoveToLocation(new Vector2(ghostController.PacMan.position.x, ghostController.PacMan.position.y));
    }

    // Patrol between a list of Way Points
    private void ClydeChase()
    {
        System.Random random = new System.Random();
        Vector3 position = patrolPoints[random.Next(0, patrolPoints.Count)];
        ghostController.SetMoveToLocation(new Vector2(position.x, position.y));
    }

    // Get in front of Pacman
    private void InkyChase()
    {
        Vector2 deltaPosition = new Vector2();
        Vector2 currentPacman = new Vector2(ghostController.PacMan.position.x, ghostController.PacMan.position.y);

        switch (ghostController.PacMan.gameObject.GetComponent<PacmanController>().moveDirection)
        {
            case PacmanController.MoveDirection.Up:
                deltaPosition = new Vector2(0, 1 * predictOffset);
                break;
            case PacmanController.MoveDirection.Down:
                deltaPosition = new Vector2(0, -1 * predictOffset);
                break;
            case PacmanController.MoveDirection.Left:
                deltaPosition = new Vector2(1 * predictOffset, 0);
                break;
            case PacmanController.MoveDirection.Right:
                deltaPosition = new Vector2(-1 * predictOffset, 0);
                break;
            default:
                break;
        }

        ghostController.SetMoveToLocation(currentPacman + deltaPosition);
    }

    // Random wander in the map but try to keep away from Blinky
    private void PinkyChase()
    {
        System.Random random = new System.Random();
        Vector3 blinkyPosition = GameObject.Find("Blinky").transform.position;
        Vector3 position = new Vector3(random.Next((int)mapTopLeft.x, (int)mapBottomRight.x + 1), random.Next((int)mapBottomRight.y, (int)mapTopLeft.y + 1), 0);
        while (Vector3.Distance(position, new Vector3(blinkyPosition.x, blinkyPosition.y, 0)) < distance )
        {
            position = new Vector3(random.Next((int)mapTopLeft.x, (int)mapBottomRight.x + 1), random.Next((int)mapBottomRight.y, (int)mapTopLeft.y + 1), 0);
        }
        ghostController.SetMoveToLocation(new Vector2(position.x, position.y));
    }
}
