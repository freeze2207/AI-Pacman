using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunawayState : GhostBaseState
{
    private float normalSpeed;

    public float awayDistance = 4.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController.state = GhostController.GhostState.Runaway;
        normalSpeed = ghostController.speed;
        ghostController.speed *= 1.75f;
        ghostController.path.Clear();
        ghostController.pathIndex = 0;

        switch (ghostController.gameObject.name)
        {
            case "Blinky":
                ghostController.pathCompletedEvent.AddListener(BlinkyRunaway);
                break;
            case "Clyde":
                ghostController.pathCompletedEvent.AddListener(ClydeRunaway);
                break;
            case "Inky":
                ghostController.pathCompletedEvent.AddListener(InkyRunaway);
                break;
            case "Pinky":
                ghostController.pathCompletedEvent.AddListener(PinkyRunaway);
                break;
            default:
                break;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (ghostController.gameObject.name == "Inky" && Vector3.Distance(ghostController.gameObject.transform.position, ghostController.PacMan.position) < awayDistance)
        {
            ghostController.path.Clear();
            ghostController.pathIndex = 0;
            InkyRunaway();
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController.speed = normalSpeed;
        animator.SetBool("IsRunaway", false);
    }

    // Flee to spawn point
    private void BlinkyRunaway()
    {
        ghostController.SetMoveToLocation(ghostController.ReturnLocation);
    }

    // Back to his own Scatter point
    private void ClydeRunaway()
    {
        System.Random random = new System.Random();
        ghostController.SetMoveToLocation(ghostController.ScatterLocation + new Vector2(0 + -1 * random.Next(-2, 3), 0 + -1 * random.Next(-2, 3)));
    }

    // Keep away from Pacman based on the direction of Pacman
    private void InkyRunaway()
    {
        Vector2 deltaPosition = new Vector2();
        if (Vector3.Distance(ghostController.gameObject.transform.position, ghostController.PacMan.position) < awayDistance)
        {
            switch (ghostController.PacMan.gameObject.GetComponent<PacmanController>().moveDirection)
            {
                case PacmanController.MoveDirection.Up:
                    deltaPosition = new Vector2(0, 1 * awayDistance);
                    break;
                case PacmanController.MoveDirection.Down:
                    deltaPosition = new Vector2(0, -1 * awayDistance);
                    break;
                case PacmanController.MoveDirection.Left:
                    deltaPosition = new Vector2(1 * awayDistance, 0);
                    break;
                case PacmanController.MoveDirection.Right:
                    deltaPosition = new Vector2(-1 * awayDistance, 0);
                    break;
                default:
                    break;
            }
            ghostController.SetMoveToLocation(new Vector2(ghostController.gameObject.transform.position.x, ghostController.gameObject.transform.position.y) + deltaPosition);
        }
    }

    // Move to the mirro position of the Pacman
    private void PinkyRunaway()
    {
        ghostController.SetMoveToLocation(new Vector2(-1 * ghostController.PacMan.transform.position.x, -1 * ghostController.PacMan.transform.position.y));
    }
}
