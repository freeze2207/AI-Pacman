using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScatterState : GhostBaseState
{
    public float scatterTime = 6.0f;
    private float currentLength = 0.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        currentLength = 0.0f;
        ghostController.state = GhostController.GhostState.Scatter;
        ghostController.SetMoveToLocation(ghostController.ScatterLocation);
        ghostController.pathCompletedEvent.AddListener(Patrol);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        currentLength += Time.deltaTime;
        if (currentLength > scatterTime)
        {
            animator.SetBool("IsChase", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController.pathCompletedEvent.RemoveListener(Patrol);
        animator.SetBool("IsScatter", false);
    }

    private void Patrol()
    {
        System.Random random = new System.Random();
        ghostController.SetMoveToLocation(ghostController.ScatterLocation + new Vector2(0 + -1 * random.Next(-2, 3), 0 + -1 * random.Next(-2, 3)));
    }
}
