using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnState : GhostBaseState
{
    private float respawnTime = 2.0f;
    private float currentLength = 0.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        currentLength = 0.0f;
        ghostController.state = GhostController.GhostState.Respawn;
        ghostController.gameObject.GetComponent<CircleCollider2D>().enabled = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        currentLength += Time.deltaTime;
        if (currentLength > respawnTime)
        {
            animator.SetBool("IsScatter", true);
        }

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsRespawn", false);
    }
}
