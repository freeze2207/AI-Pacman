using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnState : GhostBaseState
{
    private float normalSpeed;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController.state = GhostController.GhostState.ReturnHome;
        normalSpeed = ghostController.speed;
        ghostController.speed *= 0.5f; 
        ghostController.SetMoveToLocation(ghostController.ReturnLocation);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (ghostController.gameObject.transform.position.x == 0 && ghostController.gameObject.transform.position.y == 0)
        {
            animator.SetBool("IsRespawn", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ghostController.speed = normalSpeed;
        animator.SetBool("IsReturnHome", false);
    }
}
